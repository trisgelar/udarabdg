import uvicorn
import string
from fastapi import FastAPI
from services.users.users_route import router as users_router
from services.angin.angin_route import router as angin_router
from services.udara.udara_route import router as udara_router
from services.suhu.suhu_route import router as suhu_router
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["*"]
 
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users_router)
app.include_router(angin_router)
app.include_router(udara_router)
app.include_router(suhu_router)

@app.get("/")
async def read_main():
    return {"message": "Keadaan Udara Bandung API"}

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=5000, reload=True, debug=True, log_level="debug")