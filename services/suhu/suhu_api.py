from services.suhu.models.suhu_model import Suhu as db
from bson import ObjectId
from fastapi.logger import logger

db = db()

def objIdToStr(obj):
    return str(obj["id"])

def search_suhu():
    result = []
    for items in db.showSuhu():
        print(items.to_json())
        data = {
            "id" : objIdToStr(items),
            "tahun" : items.tahun,
            "bulan" : items.bulan,
            "temperatur_rataan" : items.temperatur_rataan,
            "temperatur_maks" : items.temperatur_maks,
            "temperatur_min" : items.temperatur_min,
            "curah_hujan" : items.curah_hujan,
            "hari_hujan" : items.hari_hujan,
            "lpm" : items.lpm,

        }
        result.append(data) 
    return result

def search_suhu_by_tahun_bulan(**params):
    result = db.showSuhuByTahunBulan(**params)
    if result is not None:
        result.id = objIdToStr(result)      
        return result.to_json()
    else:
        return {"message":"Data Suhu pada Bulan dan Tahun tidak ditemukan"}

def insert_suhu(**params):
    params['id'] = db.showSuhuByTahunBulan(**params)
    if params['id'] is None:
        result = db.insertSuhu(**params)
        if result is not None:      
            return result.to_json()
        else:
            return {"message":"Terjadi Error"}.to_json()
    else:
        return {"message":"Data Suhu pada Bulan dan Tahun sudah ada sebelumnya"}

def update_suhu(**params):
    params['id'] = db.showSuhuByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.updateSuhuById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Suhu pada Bulan dan Tahun tidak ditemukan"}

def delete_suhu(**params):
    params['id'] = db.showSuhuByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.deleteSuhuById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Suhu pada Bulan dan Tahun tidak ditemukan"}