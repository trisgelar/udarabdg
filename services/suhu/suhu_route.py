from fastapi import APIRouter, Body, Depends
from services.suhu.suhu_api import *
from services.users.auth_bearer import JWTBearer

router = APIRouter()

@router.put("/suhuupdate", dependencies=[Depends(JWTBearer())], tags=["suhu"])
async def view_update_suhu(params:dict):
    result = update_suhu(**params)
    return result

@router.delete("/suhudelete", dependencies=[Depends(JWTBearer())], tags=["suhu"])
async def view_delete_suhu(params:dict):
    result = delete_suhu(**params)
    return result

@router.post("/suhuinsert", dependencies=[Depends(JWTBearer())], tags=["suhu"])
async def view_insert_suhu(params:dict):
    result = insert_suhu(**params)
    return result

@router.get("/suhu", dependencies=[Depends(JWTBearer())], tags=["suhu"])
async def view_search_suhu():
    result = search_suhu()
    return result