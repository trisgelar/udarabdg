from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument, FloatField, IntField
from bson.objectid import ObjectId

try:
    connection = connect(db="udarabdg", host="localhost", port=27017)
    print("database connect euy")
except Exception as e:
    print(e)

class Suhu(Document):
    tahun = StringField(required=True, max_length=25)
    bulan = StringField(required=True, max_length=25)
    temperatur_rataan = FloatField(required=True)
    temperatur_maks = FloatField(required=True)
    temperatur_min = FloatField(required=True)
    curah_hujan = FloatField(required=True)
    hari_hujan = IntField(required=True)
    lpm = FloatField(required=True)


    def showSuhu(self):
        return Suhu.objects

    def showSuhuByTahunBulan(self, **params):
        return Suhu.objects(tahun=params['tahun'], bulan=params['bulan']).first()

    def insertSuhu(self, **params):
        try:
            result = Suhu(**params).save()
            return result
        except Exception as e:
            print(e)

    def updateSuhuById(self, **params):
        try:
            result = Suhu.objects(id=ObjectId(params['id'])).first()
            if ('temperatur_rataan' in params):
                result.temperatur_rataan = params['temperatur_rataan']
            if ('temperatur_maks' in params):
                result.temperatur_maks = params['temperatur_maks']
            if ('temperatur_min' in params):
                result.temperatur_min = params['temperatur_min']
            if ('curah_hujan' in params):
                result.curah_hujan = params['curah_hujan']
            if ('hari_hujan' in params):
                result.hari_hujan = params['hari_hujan']
            if ('lpm' in params):
                result.lpm = params['lpm']            
            result.save()
            return result
        except Exception as e:
            print(e)

    def deleteSuhuById(self, **params):
        try:
            result = Suhu.objects(id=ObjectId(params['id'])).first()
            result.delete()
            return result
        except Exception as e:
            print(e)