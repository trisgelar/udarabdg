from fastapi import APIRouter, Body, Depends
from services.users.users_api import *
from services.users.auth_bearer import JWTBearer

router = APIRouter()

@router.put("/userupdate", dependencies=[Depends(JWTBearer())], tags=["users"])
async def view_update_user(params:dict):
    result = update_user(**params)
    return result

@router.delete("/userdelete", dependencies=[Depends(JWTBearer())], tags=["users"])
async def view_delete_user(params:dict):
    result = delete_user(**params)
    return result

@router.post("/userinsert", dependencies=[Depends(JWTBearer())], tags=["users"])
async def view_insert_user(params:dict):
    result = insert_user(**params)
    return result

@router.post("/userbyemail", dependencies=[Depends(JWTBearer())], tags=["users"])
async def view_search_user_by_email(params:dict):
    result = search_user_by_email(**params)
    return result

@router.post("/requesttoken")
async def view_token(params:dict):
    result = request_token(**params)
    return result

@router.get("/users", dependencies=[Depends(JWTBearer())], tags=["users"])
async def view_search_users():
    result = search_users()
    return result