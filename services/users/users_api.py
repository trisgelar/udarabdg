from services.users.models.users_model import Users as db
from bson import ObjectId
from fastapi.logger import logger
from services.users.auth_handler import signJWT, decodeJWT

db = db()

def objIdToStr(obj):
    return str(obj["id"])

def search_users():
    result = []
    for items in db.showUsers():
        print(items.to_json())
        user = {
            "id" : objIdToStr(items),
            "username" : items.username,
            "name" : items.name,
            "email" : items.email,
            "address" : items.address,
        }
        result.append(user) 
    return result

def search_user_by_email(**params):
    result = db.showUserByEmail(**params)
    if result is not None:
        result.id = objIdToStr(result)      
        return result.to_json()
    else:
        return {"message":"Email tidak terdaftar"}

def insert_user(**params):
    result = db.insertUser(**params)
    if result is not None:      
        return result.to_json()
    else:
        return {"message":"Terjadi Error"}

def update_user(**params):
    params['id'] = db.showUserByEmail(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.updateUserById(**params)      
        return result.to_json()
    else:
        return {"message":"Email tidak terdaftar"}

def delete_user(**params):
    params['id'] = db.showUserByEmail(**params)

    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.deleteUserById(**params)      
        return result.to_json()
    else:
        return {"message":"Email tidak terdaftar"}


def request_token(**params):

    dbresult = db.showUserByEmail(**params)
    if dbresult is not None:
        return signJWT(dbresult.email)
    else:
        return { "message":"Email tidak terdaftar" }