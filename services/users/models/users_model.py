from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument
from bson.objectid import ObjectId

try:
    connection = connect(db="udarabdg", host="localhost", port=27017)
    print("database connect euy")
except Exception as e:
    print(e)

class Users(Document):
    name = StringField(required=True, max_length=50)
    username = StringField(required=True, max_length=70)
    email = StringField(required=True, max_length=30)
    address = StringField(required=True, max_length=255)

    def showUsers(self):
        return Users.objects

    def showUserByEmail(self, **params):
        return Users.objects(email=params['email']).first()

    def showUserByName(self, **params):
        return Users.objects(name=params['name']).first()

    def insertUser(self, **params):
        try:
            result = Users(**params).save()
            return result
        except Exception as e:
            print(e)

    def updateUserById(self, **params):
        try:
            result = Users.objects(id=ObjectId(params['id'])).first()
            if ('username' in params):
                result.username = params['username']
            if ('name' in params):
                result.name = params['name']
            if ('email' in params):
                result.email = params['email']
            result.save()
            return result
        except Exception as e:
            print(e)

    def deleteUserById(self, **params):
        try:
            result = Users.objects(id=ObjectId(params['id'])).first()
            result.delete()
            return result
        except Exception as e:
            print(e)
    




