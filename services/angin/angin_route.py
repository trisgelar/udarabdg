from fastapi import APIRouter, Body, Depends
from services.angin.angin_api import *
from services.users.auth_bearer import JWTBearer

router = APIRouter()

@router.put("/anginupdate", dependencies=[Depends(JWTBearer())], tags=["angin"])
async def view_update_angin(params:dict):
    result = update_angin(**params)
    return result

@router.delete("/angindelete", dependencies=[Depends(JWTBearer())], tags=["angin"])
async def view_delete_angin(params:dict):
    result = delete_angin(**params)
    return result

@router.post("/angininsert", dependencies=[Depends(JWTBearer())], tags=["angin"])
async def view_insert_angin(params:dict):
    result = insert_angin(**params)
    return result

@router.get("/angin", dependencies=[Depends(JWTBearer())], tags=["angin"])
async def view_search_angin():
    result = search_angin()
    return result