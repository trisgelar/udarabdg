from services.angin.models.angin_model import Angin as db
from bson import ObjectId
from fastapi.logger import logger

db = db()

def objIdToStr(obj):
    return str(obj["id"])

def search_angin():
    result = []
    for items in db.showAngin():
        print(items.to_json())
        data = {
            "id" : objIdToStr(items),
            "tahun" : items.tahun,
            "bulan" : items.bulan,
            "kecepatan_angin_rataan" : items.kecepatan_angin_rataan,
            "kecepatan_angin_maks" : items.kecepatan_angin_maks,
        }
        result.append(data) 
    return result

def search_angin_by_tahun_bulan(**params):
    result = db.showAnginByTahunBulan(**params)
    if result is not None:
        result.id = objIdToStr(result)      
        return result.to_json()
    else:
        return {"message":"Data Angin pada Bulan dan Tahun tidak ditemukan"}

def insert_angin(**params):
    params['id'] = db.showAnginByTahunBulan(**params)
    if params['id'] is None:
        result = db.insertAngin(**params)
        if result is not None:      
            return result.to_json()
        else:
            return {"message":"Terjadi Error"}
    else:
        return {"message":"Data Angin pada Bulan dan Tahun sudah ada sebelumnya"}

def update_angin(**params):
    params['id'] = db.showAnginByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.updateAnginById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Angin pada Bulan dan Tahun tidak ditemukan"}

def delete_angin(**params):
    params['id'] = db.showAnginByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.deleteAnginById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Angin pada Bulan dan Tahun tidak ditemukan"}