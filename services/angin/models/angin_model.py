from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument, FloatField
from bson.objectid import ObjectId

try:
    connection = connect(db="udarabdg", host="localhost", port=27017)
    print("database connect euy")
except Exception as e:
    print(e)

class Angin(Document):
    tahun = StringField(required=True, max_length=25)
    bulan = StringField(required=True, max_length=25)
    kecepatan_angin_rataan = FloatField(required=True)
    kecepatan_angin_maks = FloatField(required=True)

    def showAngin(self):
        return Angin.objects

    def showAnginByTahunBulan(self, **params):
        return Angin.objects(tahun=params['tahun'], bulan=params['bulan']).first()

    def insertAngin(self, **params):
        try:
            result = Angin(**params).save()
            return result
        except Exception as e:
            print(e)

    def updateAnginById(self, **params):
        try:
            result = Angin.objects(id=ObjectId(params['id'])).first()
            if ('kecepatan_angin_rataan' in params):
                result.kecepatan_angin_rataan = params['kecepatan_angin_rataan']
            if ('kecepatan_angin_maks' in params):
                result.kecepatan_angin_maks = params['kecepatan_angin_maks']
            result.save()
            return result
        except Exception as e:
            print(e)

    def deleteAnginById(self, **params):
        try:
            result = Angin.objects(id=ObjectId(params['id'])).first()
            result.delete()
            return result
        except Exception as e:
            print(e)
    




