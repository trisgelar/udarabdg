from fastapi import APIRouter, Body, Depends
from services.udara.udara_api import *
from services.users.auth_bearer import JWTBearer

router = APIRouter()

@router.put("/udaraupdate", dependencies=[Depends(JWTBearer())], tags=["udara"])
async def view_update_udara(params:dict):
    result = update_udara(**params)
    return result

@router.delete("/udaradelete", dependencies=[Depends(JWTBearer())], tags=["udara"])
async def view_delete_udara(params:dict):
    result = delete_udara(**params)
    return result

@router.post("/udarainsert", dependencies=[Depends(JWTBearer())], tags=["udara"])
async def view_insert_udara(params:dict):
    result = insert_udara(**params)
    return result

@router.get("/udara", dependencies=[Depends(JWTBearer())], tags=["udara"])
async def view_search_udara():
    result = search_udara()
    return result