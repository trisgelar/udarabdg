from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument, FloatField
from bson.objectid import ObjectId

try:
    connection = connect(db="udarabdg", host="localhost", port=27017)
    print("database connect euy")
except Exception as e:
    print(e)

class Udara(Document):
    tahun = StringField(required=True, max_length=25)
    bulan = StringField(required=True, max_length=25)
    penguapan = FloatField(required=True)
    tekanan_udara = FloatField(required=True)
    kelembapan = FloatField(required=True)

    def showUdara(self):
        return Udara.objects

    def showUdaraByTahunBulan(self, **params):
        return Udara.objects(tahun=params['tahun'], bulan=params['bulan']).first()

    def insertUdara(self, **params):
        try:
            result = Udara(**params).save()
            return result
        except Exception as e:
            print(e)

    def updateUdaraById(self, **params):
        try:
            result = Udara.objects(id=ObjectId(params['id'])).first()
            if ('penguapan' in params):
                result.penguapan = params['penguapan']
            if ('tekanan_udara' in params):
                result.tekanan_udara = params['tekanan_udara']
            if ('kelembapan' in params):
                result.kelembapan = params['kelembapan']
            result.save()
            return result
        except Exception as e:
            print(e)

    def deleteUdaraById(self, **params):
        try:
            result = Udara.objects(id=ObjectId(params['id'])).first()
            result.delete()
            return result
        except Exception as e:
            print(e)