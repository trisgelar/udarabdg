from services.udara.models.udara_model import Udara as db
from bson import ObjectId
from fastapi.logger import logger

db = db()

def objIdToStr(obj):
    return str(obj["id"])

def search_udara():
    result = []
    for items in db.showUdara():
        print(items.to_json())
        data = {
            "id" : objIdToStr(items),
            "tahun" : items.tahun,
            "bulan" : items.bulan,
            "penguapan" : items.penguapan,
            "tekanan_udara" : items.tekanan_udara,
            "kelembapan" : items.kelembapan,
        }
        result.append(data) 
    return result

def search_udara_by_tahun_bulan(**params):
    result = db.showUdaraByTahunBulan(**params)
    if result is not None:
        result.id = objIdToStr(result)      
        return result.to_json()
    else:
        return {"message":"Data Udara pada Bulan dan Tahun tidak ditemukan"}

def insert_udara(**params):
    params['id'] = db.showUdaraByTahunBulan(**params)
    if params['id'] is None:
        result = db.insertUdara(**params)
        if result is not None:      
            return result.to_json()
        else:
            return {"message":"Terjadi Error"}.to_json()
    else:
        return {"message":"Data Udara pada Bulan dan Tahun sudah ada sebelumnya"}

def update_udara(**params):
    params['id'] = db.showUdaraByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.updateUdaraById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Udara pada Bulan dan Tahun tidak ditemukan"}

def delete_udara(**params):
    params['id'] = db.showUdaraByTahunBulan(**params)
    if params['id'] is not None:
        params['id'] = objIdToStr(params['id'])
        result = db.deleteUdaraById(**params)      
        return result.to_json()
    else:
        return {"message":"Data Udara pada Bulan dan Tahun tidak ditemukan"}