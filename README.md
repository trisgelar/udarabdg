# udarabdg

Fast API Keadaan udara di Kota Bandung

## Deskripsi

### Dataset
1. Keadaan Udara [data](http://data.bandung.go.id/dataset/keadaan-udara-menurut-bulan-di-kota-bandung)
2. Suhu dan Kelembapan Udara [data](http://data.bandung.go.id/dataset/rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung)
3. Kecepatan Angin [data](http://data.bandung.go.id/dataset/kecepatan-angin-menurut-bulan-di-kota-bandung)

```sh
(sanber) ➜  dataset git:(main) ✗ tree
.
├── angin
│ ├── kecepatan-angin-kota-bandung-2013.csv
│ ├── kecepatan-angin-kota-bandung-2014.csv
│ ├── kecepatan-angin-kota-bandung-2015.csv
│ ├── kecepatan-angin-kota-bandung-2016.csv
│ ├── kecepatan-angin-kota-bandung-2017.csv
│ ├── kecepatan-angin-kota-bandung-2018.csv
│ └── kecepatan-angin-kota-bandung-2019.csv
├── suhu
│ ├── rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung-2015.csv
│ ├── rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung-2016.csv
│ ├── rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung-2017.csv
│ ├── rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung-2018.csv
│ └── rata-rata-suhu-dan-kelembaban-udara-menurut-bulan-di-kota-bandung-2019.csv
└── udara
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2013.csv
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2014.csv
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2015.csv
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2016.csv
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2017.csv
    ├── keadaan-udara-menurut-bulan-di-kota-bandung-2018.csv
    └── keadaan-udara-menurut-bulan-di-kota-bandung-2019.csv
```

### Inisiasi MongoDB Database
1. Buatlah DB udarabdg
2. Buatlah 4 Collection yaitu users, udara, suhu, dan angin
3. Import data ke collection sesuai filename pada folder backup/udarabdg

```sh Data Dummy
(sanber) ➜  backup git:(main) ✗ tree
.
└── udarabdg
    ├── angin.json
    ├── suhu.json
    ├── udara.json
    └── users.json
```

### RESTAPI

1. User Services
2. Keadaan Udara Services
3. Kecepatan Angin Services
4. Suhu Services

### Contoh Pemakaian
1. export backup/postman/udarabdg.postman_collection.json ke postman
2. jalankan requesttoken api pada users/FastAPIJWTRequestToken
3. ekslorasi API users, udara, angin dan suhu

## Pengembangan

### JWT
Menggunakan pyJWT untuk menggenerate token (encode) dan decode token
Menggunakan HTTPBearer dari fastapi.security sebagai decorator yang memverifikasi jwt token
Sumber Tutorial [fastapi-jwt-auth](https://testdriven.io/blog/fastapi-jwt-auth/)

### ORM
Menggunakan MongoEngine


### Install
pip install fastapi-security pyJWT

